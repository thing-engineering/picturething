FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

#RUN apt-get update && apt-get install -y build-essential nodejs npm

WORKDIR /app
COPY . .
#RUN dotnet test

WORKDIR /app/src/PictureThing.Web
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/src/PictureThing.Web/out ./

ENV ASPNETCORE_URLS "http://0:5000"
EXPOSE 5000
ENTRYPOINT ["dotnet", "PictureThing.Web.dll"]
