﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PictureThing.Common.Repositories;
using PictureThing.Web.Misc;
using PictureThing.Web.ViewModels;

namespace PictureThing.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPictureRepository _repository;

        public HomeController(ILogger<HomeController> logger, IPictureRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IActionResult> Index(string path)
        {
            path = (path ?? "").TrimEnd('/');
            _logger.LogDebug($"path: {path}");

            var folder = await _repository.GetFolderBySiteIdAndPath(SiteContext.ActiveSite.Id, path);
            if (folder == null)
            {
                //return NotFound();
            }

            foreach (var b in Breadcrumb.FromPath(path))
            {
                _logger.LogDebug($"crumb: {b.Url} => {b.Name}");
            }
            _logger.LogDebug(string.Join(" / ", path.Split('/')));

            var viewModel = new IndexViewModel
            {
                Breadcrumbs = Breadcrumb.FromPath(path),
                Folders = await _repository.GetFoldersByFolderId(folder?.Id ?? 0),
                Pictures = await _repository.GetPicturesByFolderId(folder?.Id ?? 0),
            };

            return View(viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
