﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PictureThing.Web.Misc
{
    public class Breadcrumb
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public static Breadcrumb Root = new Breadcrumb("[Root]", "/");

        public Breadcrumb(string name, string url)
        {
            Name = name;
            Url = url;
        }

        public static List<Breadcrumb> FromPath(string path)
        {
            var ret = new List<Breadcrumb> { Root };
            string url = "";
            foreach (string pathPart in path.Split('/'))
            {
                url = $"{url}/{pathPart}";
                ret.Add(new Breadcrumb(pathPart, url));
            }
            return ret;
        }
    }
}
