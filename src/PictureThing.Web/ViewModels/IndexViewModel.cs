﻿using PictureThing.Common.Models;
using PictureThing.Web.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PictureThing.Web.ViewModels
{
    public class IndexViewModel
    {
        public List<Breadcrumb> Breadcrumbs { get; set; }
        public List<Folder> Folders { get; set; }
        public List<Picture> Pictures { get; set; }
    }
}
