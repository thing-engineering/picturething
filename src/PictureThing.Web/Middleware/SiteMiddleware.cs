﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PictureThing.Common.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PictureThing.Web.Middleware
{
    public class SiteMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public SiteMiddleware(RequestDelegate next, ILogger<SiteMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext, IPictureRepository repository)
        {
            var sw = Stopwatch.StartNew();

            SiteContext.ActiveSite = await repository.GetSiteByHost(httpContext.Request.Host.Host);

            sw.Stop();
            _logger.LogDebug("Invoke took {0}ms", sw.ElapsedMilliseconds);

            // Call the next middleware
            await _next(httpContext);
        }
    }
}
