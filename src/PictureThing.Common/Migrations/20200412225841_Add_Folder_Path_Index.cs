﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PictureThing.Common.Migrations
{
    public partial class Add_Folder_Path_Index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_folders_site_id",
                table: "folders");

            migrationBuilder.CreateIndex(
                name: "ix_folders_site_id_full_path",
                table: "folders",
                columns: new[] { "site_id", "full_path" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_folders_site_id_full_path",
                table: "folders");

            migrationBuilder.CreateIndex(
                name: "ix_folders_site_id",
                table: "folders",
                column: "site_id");
        }
    }
}
