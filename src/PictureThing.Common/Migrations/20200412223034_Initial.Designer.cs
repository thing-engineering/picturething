﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using PictureThing.Common.Contexts;

namespace PictureThing.Common.Migrations
{
    [DbContext(typeof(PictureDbContext))]
    [Migration("20200412223034_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("PictureThing.Common.Models.Folder", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("FullPath")
                        .HasColumnName("full_path")
                        .HasColumnType("text");

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("text");

                    b.Property<int?>("ParentId")
                        .HasColumnName("parent_id")
                        .HasColumnType("integer");

                    b.Property<int>("SiteId")
                        .HasColumnName("site_id")
                        .HasColumnType("integer");

                    b.HasKey("Id")
                        .HasName("pk_folders");

                    b.HasIndex("ParentId")
                        .HasName("ix_folders_parent_id");

                    b.HasIndex("SiteId")
                        .HasName("ix_folders_site_id");

                    b.ToTable("folders");
                });

            modelBuilder.Entity("PictureThing.Common.Models.Picture", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("FileName")
                        .HasColumnName("file_name")
                        .HasColumnType("text");

                    b.Property<int>("FolderId")
                        .HasColumnName("folder_id")
                        .HasColumnType("integer");

                    b.Property<int>("Height")
                        .HasColumnName("height")
                        .HasColumnType("integer");

                    b.Property<int>("ThumbnailHeight")
                        .HasColumnName("thumbnail_height")
                        .HasColumnType("integer");

                    b.Property<string>("ThumbnailName")
                        .HasColumnName("thumbnail_name")
                        .HasColumnType("text");

                    b.Property<int>("ThumbnailWidth")
                        .HasColumnName("thumbnail_width")
                        .HasColumnType("integer");

                    b.Property<int>("Width")
                        .HasColumnName("width")
                        .HasColumnType("integer");

                    b.HasKey("Id")
                        .HasName("pk_pictures");

                    b.HasIndex("FolderId")
                        .HasName("ix_pictures_folder_id");

                    b.ToTable("pictures");
                });

            modelBuilder.Entity("PictureThing.Common.Models.Site", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Host")
                        .HasColumnName("host")
                        .HasColumnType("text");

                    b.Property<string>("ImageUri")
                        .HasColumnName("image_uri")
                        .HasColumnType("text");

                    b.Property<string>("Title")
                        .HasColumnName("title")
                        .HasColumnType("text");

                    b.HasKey("Id")
                        .HasName("pk_sites");

                    b.ToTable("sites");
                });

            modelBuilder.Entity("PictureThing.Common.Models.Folder", b =>
                {
                    b.HasOne("PictureThing.Common.Models.Folder", "Parent")
                        .WithMany()
                        .HasForeignKey("ParentId")
                        .HasConstraintName("fk_folders_folders_parent_id");

                    b.HasOne("PictureThing.Common.Models.Site", "Site")
                        .WithMany()
                        .HasForeignKey("SiteId")
                        .HasConstraintName("fk_folders_sites_site_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("PictureThing.Common.Models.Picture", b =>
                {
                    b.HasOne("PictureThing.Common.Models.Folder", "Folder")
                        .WithMany()
                        .HasForeignKey("FolderId")
                        .HasConstraintName("fk_pictures_folders_folder_id")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
