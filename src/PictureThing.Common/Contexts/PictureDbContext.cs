﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using PictureThing.Common.Models;

namespace PictureThing.Common.Contexts
{
    public class PictureDbContext : DbContext
    {
        public DbSet<Folder> Folders { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Site> Sites { get; set; }

        private readonly string _connectionString;

        public PictureDbContext(string connectionString) : base()
        {
            _connectionString = connectionString;
        }

        public PictureDbContext(DbContextOptions<PictureDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseNpgsql(_connectionString)
                    .UseSnakeCaseNamingConvention();
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Indexes
            builder.Entity<Folder>()
                .HasIndex(e => new { e.SiteId, e.FullPath });
        }
    }
}
