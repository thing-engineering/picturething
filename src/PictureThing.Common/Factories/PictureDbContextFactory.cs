﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using PictureThing.Common.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Database.Web.Factories
{
    public class PictureDbContextFactory : IDesignTimeDbContextFactory<PictureDbContext>
    {
        public PictureDbContext CreateDbContext(string[] args) => new PictureDbContext("Host=localhost;Port=51002;Username=picturething;Password=topsecret");
    }
}
