﻿using PictureThing.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureThing.Common.Repositories
{
    public interface IPictureRepository
    {
        Task Initialize();

        Task<Site> GetSiteByHost(string host);
        Task<Folder> GetFolderBySiteIdAndPath(int siteId, string path);
        Task<List<Folder>> GetFoldersByFolderId(int folderId);
        Task<List<Picture>> GetPicturesByFolderId(int folderId);
    }
}
