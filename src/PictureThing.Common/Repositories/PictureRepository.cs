﻿using Microsoft.EntityFrameworkCore;
using PictureThing.Common.Contexts;
using PictureThing.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PictureThing.Common.Repositories
{
    public class PictureRepository : IPictureRepository
    {
        protected readonly PictureDbContext _context;

        public PictureRepository(PictureDbContext context)
        {
            _context = context;
        }

        public virtual async Task Initialize()
        {
            //_logger.LogInformation("Making sure database migrations are up to date");
            await _context.Database.MigrateAsync();
        }

        public async Task<Site> GetSiteByHost(string host)
        {
            return await _context.Sites
                .Where(s => s.Host == host)
                .FirstAsync();
        }

        public async Task<Folder> GetFolderBySiteIdAndPath(int siteId, string path)
        {
            return await _context.Folders
                .Where(f => f.SiteId == siteId && f.FullPath == path)
                .FirstOrDefaultAsync();
        }

        public async Task<List<Folder>> GetFoldersByFolderId(int folderId)
        {
            return await _context.Folders
                .Where(f => f.ParentId == folderId)
                .OrderBy(f => f.Name)
                .ToListAsync();
        }

        public async Task<List<Picture>> GetPicturesByFolderId(int folderId)
        {
            return await _context.Pictures
                .Where(p => p.FolderId == folderId)
                .OrderBy(p => p.FileName)
                .ToListAsync();
        }
    }
}
