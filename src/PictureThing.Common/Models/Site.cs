﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PictureThing.Common.Models
{
    public class Site
    {
        public int Id { get; set; }

        public string Host { get; set; }
        public string Title { get; set; }
        public string ImageUri { get; set; }
    }
}
