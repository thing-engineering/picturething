﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PictureThing.Common.Models
{
    public class Folder
    {
        public int Id { get; set; }

        public int SiteId { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }

        [ForeignKey("SiteId")]
        public Site Site { get; set; }
        
        [ForeignKey("ParentId")]
        public Folder Parent { get; set; }
    }
}
