﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PictureThing.Common.Models
{
    public class Picture
    {
        public long Id { get; set; }

        public int FolderId { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int ThumbnailHeight { get; set; }
        public int ThumbnailWidth { get; set; }
        public string FileName { get; set; }
        public string ThumbnailName { get; set; }

        [ForeignKey("FolderId")]
        public Folder Folder { get; set; }
    }
}
